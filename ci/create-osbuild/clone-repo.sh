#!/bin/bash

set -euxo pipefail

MAIN_REPO="https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
REPO_URL=${REPO_URL:-$MAIN_REPO}
OSBUILD_DIR=${OSBUILD_DIR:-osbuild-manifests}

if [[ "${REPO_URL}" == "${MAIN_REPO}" ]]; then
    REPO_URL="https://gitlab.com/CentOS/automotive/sample-images.git"
    REVISION="main"
fi

if ! [[ -v REVISION ]]; then
    echo "Error: The ENV variable REVISION is missing"
    exit 1
fi

echo "[+] Install Git"
sudo dnf install -y git

echo "[+] Remove any previous manifests"
rm -vfr "${OSBUILD_DIR}"
echo "[+] Clone the repository with the manifests"
if [[ "${SSL_VERIFY}" == "false" ]]; then
    GIT_SSL_NO_VERIFY=true git clone "${REPO_URL}" "${OSBUILD_DIR}"
else
    git clone "${REPO_URL}" "${OSBUILD_DIR}"
fi
cd "${OSBUILD_DIR}"
git reset --hard "${REVISION}"

echo "[+] Cloned"
