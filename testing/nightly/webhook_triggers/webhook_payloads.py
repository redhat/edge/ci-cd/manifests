#! /usr/bin/python3
import os
import json

import sh
from typing import Tuple


class webhook_payload:
    """This class represents a mock gitlab webhook request"""

    def __init__(self, endpoint=None, header=None, data=None):
        self.endpoint = endpoint
        self.header = header
        self.data = data


def get_before_after_hash(repo: str) -> Tuple[str, str]:

    repo_dir = repo.split(".")[-2].split("/")[-1]

    sh.git.clone(repo)
    sh.git.fetch("--all", _cwd=repo_dir)

    before = sh.git("rev-parse", "HEAD~1", _cwd=repo_dir)
    after = sh.git("rev-parse", "HEAD", _cwd=repo_dir)

    return before.strip(), after.strip()


nightly_webhook_endpoint = os.environ.get("NIGHTLY_WEBHOOK_ENDPOINT")
nightly_git_ssh_url = os.environ.get("NIGHTLY_GIT_SSH_URL")
nightly_git_http_url = os.environ.get("NIGHTLY_GIT_HTTP_URL")
nightly_git_project_id = os.environ.get("NIGHTLY_GIT_PROJECT_ID")

header_tag = {
    "X-Gitlab-Event": "Tag Push Hook",
    "Content-Type": "application/json",
}


def get_data_tag():

    _, after = get_before_after_hash(nightly_git_http_url)

    return json.dumps(
        {
            "object_kind": "tag_push",
            "event_name": "tag_push",
            "before": "0000000000000000000000000000000000000000",
            "after": after,
            "ref": "refs/tags/nightly",
            "user_email": "automotive-a-team@redhat.com",
            "project_id": nightly_git_project_id,
            "project": {
                "id": nightly_git_project_id,
                "git_ssh_url": nightly_git_ssh_url,
                "git_http_url": nightly_git_http_url,
                "default_branch": "main",
                "url": nightly_git_ssh_url,
                "ssh_url": nightly_git_ssh_url,
                "http_url": nightly_git_http_url,
            },
            "repository": {
                "url": nightly_git_ssh_url,
                "git_http_url": nightly_git_http_url,
                "git_ssh_url": nightly_git_ssh_url,
            },
        }
    )


payloads = [
    webhook_payload(
        endpoint=nightly_webhook_endpoint,
        header=header_tag,
        data=get_data_tag(),
    ),
]
