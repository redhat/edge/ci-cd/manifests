---
productbuild:
  workspaces:
    - name: shared-workspace
    - name: ssl-ca-directory
  params: "{{ params_default }}"
  tasks:
    - name: set-developer-pipeline-product-build-running
      params: "{{ params_task + params_set_developer_pipeline_product_build_running }}"
      workspaces:
        - name: ssl-ca-directory
          workspace: ssl-ca-directory
      when:
        - input: "{{ developer }}"
          operator: in
          values: ["true"]
      taskSpec: "{{ task_gitlab_set_status }}"

    - name: ci-fetch-repository
      params:
        - name: url
          value: $(params.CI_REPO_URL)
        - name: subdirectory
          value: "toolchain"
        - name: deleteExisting
          value: "true"
        - name: revision
          value: $(params.CI_REVISION)
        - name: depth
          value: "0"
        - name: sslVerify
          value: $(params.SSL_VERIFY)
      workspaces:
        - name: output
          workspace: shared-workspace
      taskSpec: "{{ task_git_clone_ssl }}"

    - name: sig-fetch-repository
      params:
        - name: url
          value: $(params.GIT_REPO_URL)
        - name: subdirectory
          value: "manifest_repo"
        - name: deleteExisting
          value: "true"
        - name: revision
          value: "main"
        - name: depth
          value: "0"
        - name: sslVerify
          value: $(params.SSL_VERIFY)
      workspaces:
        - name: output
          workspace: shared-workspace
        - name: ssl-ca-directory
          workspace: ssl-ca-directory
      taskSpec: "{{ task_git_clone_ssl }}"

    - name: pipeline-guard
      runAfter:
        - sig-fetch-repository
        - ci-fetch-repository
      params: "{{ params_task }}"
      workspaces:
        - name: source
          workspace: shared-workspace
        - name: ssl-ca-directory
          workspace: ssl-ca-directory
      taskSpec: "{{ task_product_build_pipeline_guard }}"

    - name: set-badge-status
      runAfter:
        - pipeline-guard
      params: "{{ params_task + params_set_badge_status }}"
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      workspaces:
        - name: ssl-ca-directory
          workspace: ssl-ca-directory
      taskSpec: "{{ task_gitlab_set_badge }}"

    - name: define-build-timestamp
      runAfter:
        - pipeline-guard
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_define_build_timestamp }}"

    - name: create-yum-repo
      runAfter:
        - define-build-timestamp
      params: "{{ params_task + params_create_yum_repo }}"
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      workspaces:
        - name: source
          workspace: shared-workspace
      taskSpec: "{{ task_create_yum_repo }}"

    - name: upload-product-build
      runAfter:
        - create-yum-repo
      params: "{{ params_task + params_upload_product_build }}"
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      workspaces:
        - name: source
          workspace: shared-workspace
      taskSpec: "{{ task_upload_product_build }}"

    - name: create-osbuild-cs9-aarch64-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_cs9_aarch64_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-cs9-x86-64-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_cs9_x86_64_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-qa-minimal-ostree-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_qa_minimal_ostree_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-rpi-minimal-regular-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_rpi_minimal_regular_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-rpi-ps-aarch64-ostree-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_rpi_ostree_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
        - input: "$(params.STREAM)"
          operator: in
          values: ["downstream"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-rpi-ps-aarch64-regular-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_rpi_regular_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
        - input: "$(params.STREAM)"
          operator: in
          values: ["downstream"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: create-osbuild-qemu-ps-aarch64-regular-product-build
      runAfter:
        - upload-product-build
      params: "{{ params_task + params_create_osbuild_qemu_regular_product_build }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
        - input: "$(params.STREAM)"
          operator: in
          values: ["downstream"]
      taskSpec: "{{ task_run_in_testing_farm }}"

    - name: fusa-certification-assembly
      runAfter:
        - create-yum-repo
      params: "{{ params_task + params_task_fusa_certification_assembly }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
        - input: "$(params.STREAM)"
          operator: in
          values: ["downstream"]
      taskSpec: "{{ task_fusa_certification_assembly }}"

  finally:
    - name: upload-all-taskrun-log
      params: "{{ params_task + params_product_build_upload_logs }}"
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_upload_taskrun_log }}"

    - name: sync-latest-sample-images
      params: "{{ params_task + params_sync_latest_sample_images }}"
      workspaces:
        - name: source
          workspace: shared-workspace
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_sync_latest_sample_images }}"

    - name: create-product-build-set-badge
      params: "{{ params_task + params_create_product_build_set_badge }}"
      workspaces:
        - name: ssl-ca-directory
          workspace: ssl-ca-directory
      when:
        - input: "$(tasks.pipeline-guard.results.skip-image-build)"
          operator: notin
          values: ["yes"]
      taskSpec: "{{ task_gitlab_set_badge }}"

    - name: set-developer-pipeline-product-build-status
      params: "{{ params_task + params_set_developer_pipeline_product_build_status }}"
      workspaces:
        - name: ssl-ca-directory
          workspace: ssl-ca-directory
      when:
        - input: "{{ developer }}"
          operator: in
          values: ["true"]
      taskSpec: "{{ task_set_developer_pipeline_status }}"
